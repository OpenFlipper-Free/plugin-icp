/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2020, RWTH-Aachen University                      *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <ICPPlugin.hh>
#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;

PYBIND11_EMBEDDED_MODULE(ICP, m) {

  QObject* pluginPointer = getPluginPointer("ICP");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-ICP" << std::endl;
     return;
   }

  ICPPlugin* plugin = qobject_cast<ICPPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-ICP" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< ICPPlugin,std::unique_ptr<ICPPlugin, py::nodelete> > icp(m, "ICP");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  icp.def(py::init([plugin]() { return plugin; }));

  icp.def("icp", &ICPPlugin::icp,
    QCoreApplication::translate("PythonDocICP", "Execute the ICP algorithm").toLatin1().data(),
    py::arg(QCoreApplication::translate("PythonDocICP","Max Iteration count").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","MSE error threshold").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Point distance threshold").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Normal dot threshold in percent").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Enable debug output").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Enable auto resize").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Interpolate Normals").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Source object id").toLatin1().data()),
    py::arg(QCoreApplication::translate("PythonDocICP","Target object id").toLatin1().data())
    );
}