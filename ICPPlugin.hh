/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2020, RWTH-Aachen University                      *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#pragma once


#include <QSpinBox>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/PythonInterface.hh>
#include <OpenFlipper/BasePlugin/BackupInterface.hh>
#include <OpenFlipper/common/Types.hh>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <algorithm>
#include "ICPToolbarWidget.hh"
#include "VertexList.hh"
#include <ACG/Geometry/Algorithms.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>

#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

struct ICPParameters {
  int maxIterations_;
  double errorThreshold_; //mse
  double distanceThreshold_; //units
  double normalDotThreshold_; //% (eg. 56.3 for 56.3%)
  bool debugOutput_;
  bool doResize_;
  bool interpolateNormals_;
};

typedef struct ICPParameter ICPParameter;

struct ICPContext {
  TriMesh* sourceMesh_;
  TriMesh* targetMesh_;
  TriMeshObject* sourceObject_;
  TriMeshObject* targetObject_;
  int sourceID_;
  int targetID_;
  ICPParameters* params_;
};

typedef struct ICPContext ICPContext;

class ICPPlugin : public QObject, BaseInterface, ToolboxInterface, LoggingInterface, ScriptInterface, BackupInterface, LoadSaveInterface, PythonInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(ToolboxInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(ScriptInterface)
  Q_INTERFACES(LoadSaveInterface)
  Q_INTERFACES(BackupInterface)
  Q_INTERFACES(PythonInterface)

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-ICP")

  signals:
    //BaseInterface
    void updateView();
    void updatedObject(int _id, const UpdateType& _type);

    //LoggingInterface
    void log(Logtype _type, QString _message);
    void log(QString _message);
    
    // ToolboxInterface
    void addToolbox( QString _name  , QWidget* _widget, QIcon* _icon);
    
    // ScriptInterface
    void scriptInfo(QString _functionName);
    
    // BackupInterface
    void createBackup( int _id , QString _name, UpdateType _type = UPDATE_ALL );

    // LoadSaveInterface
    void addEmptyObject( DataType _type, int& _id);
    void deleteObject(int _id);
    void deleteAllObjects();

  private:
    ICPToolbarWidget* tool_;
    QIcon* toolIcon_;


  public:

    ICPPlugin();
    ~ICPPlugin();

    // BaseInterface
    QString name() { return (QString("ICP")); }
    QString description( ) { return (QString("Implementation of the ICP algorithm")); }

   private:

   private slots: 
    void initializePlugin(); // BaseInterface
    
    void pluginsInitialized(); // BaseInterface
    
    /**
     * Execute the iterative closest point algorithm.
     * Transform SOURCE objects to TARGET object
     */
    void toolbarICP();

    // Scriptable functions
   public slots:

    /**
     * Execute the iterative closest point algorithm.
     * Exposes all parameters for python scripting support
     */
    void icp(int _maxIterations, double _errorThreshold, double _distanceThreshold, double _normalDotThreshold, bool _debugOutput, bool _doResize, bool _interpolateNormals, int _sourceObjectID, int _targetObjectID);

    /** Executes the icp procedure.
     */
    void doICP(ICPContext* _context);

    QString version() { return QString("1.0"); }
};

